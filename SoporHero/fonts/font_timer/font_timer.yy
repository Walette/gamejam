{
    "id": "8fbe53ee-bbeb-4a94-b526-090e8bfd368e",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_timer",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Komika Axis",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "41a52702-68ab-4785-b169-aa07cf617d71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 35,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 373,
                "y": 76
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3cf31ac2-9414-4536-9c48-d201438af58a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 442,
                "y": 76
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a3b40653-86a1-44da-bd52-2e1c60bb2b5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 482,
                "y": 76
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4c94448d-3d16-4857-a9a3-5b64ca36ee49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": -2,
                "shift": 15,
                "w": 17,
                "x": 56,
                "y": 39
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "60ec57ce-a9a2-4776-95df-4135c8bc7fdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 31,
                "offset": -1,
                "shift": 12,
                "w": 11,
                "x": 222,
                "y": 76
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b632f109-248c-474b-a88e-9f895169d1fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": -1,
                "shift": 19,
                "w": 18,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9fa27aaa-192b-496f-b14f-84fe3d2f8ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": -2,
                "shift": 17,
                "w": 17,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "349600e5-3e15-457a-8c25-dabea81cbe3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 113
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "91b9eb75-abac-46f7-a4de-94da70c06a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 431,
                "y": 76
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "56b5b032-8285-42ff-a155-c31f31b97a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": -3,
                "shift": 8,
                "w": 10,
                "x": 314,
                "y": 76
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f65ca0e7-15c9-446a-b1d0-255aca00211d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 175,
                "y": 76
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b843a899-ddbc-4cd5-8d1d-14c755267caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 27,
                "offset": -2,
                "shift": 15,
                "w": 15,
                "x": 98,
                "y": 76
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "85cb72b9-2d54-48a6-af67-e50e6f85b51e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 32,
                "offset": -3,
                "shift": 7,
                "w": 7,
                "x": 464,
                "y": 76
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "91dacf36-7f75-4d05-932b-1e1587c8a9b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": -4,
                "shift": 12,
                "w": 13,
                "x": 345,
                "y": 76
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e959e8a4-2392-4994-b42f-3762c5918668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 29,
                "offset": -1,
                "shift": 6,
                "w": 5,
                "x": 504,
                "y": 76
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "47d2b97b-4991-405d-b994-3a1cc9ab7162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": -2,
                "shift": 11,
                "w": 13,
                "x": 145,
                "y": 76
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d2677670-48b6-4469-b2aa-faabf2044104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 305,
                "y": 39
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "19d91479-4ec8-4b96-a238-7b375dd70ab8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 409,
                "y": 76
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9be38307-0cfc-4e4c-8066-ad9cfd9489d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 29,
                "offset": -2,
                "shift": 13,
                "w": 15,
                "x": 492,
                "y": 39
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "63412d0e-2170-4659-a5a3-c14aa133fe42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 29,
                "offset": -2,
                "shift": 14,
                "w": 15,
                "x": 441,
                "y": 39
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "53f06825-c3de-47b0-9fbd-3be5cac07ed6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": -2,
                "shift": 15,
                "w": 16,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "79a6ba8f-e9b0-4a0b-b49a-9a57371cf7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 29,
                "offset": -2,
                "shift": 12,
                "w": 15,
                "x": 458,
                "y": 39
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9e9810a0-4a85-4c6f-b59a-f6b9e3666af2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 29,
                "offset": -1,
                "shift": 13,
                "w": 13,
                "x": 160,
                "y": 76
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fa81e4bf-82fa-4f21-9097-c5e2b0e0a103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 14,
                "x": 50,
                "y": 76
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e8fb3083-919f-43ee-ab3c-63a07803ee6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 373,
                "y": 39
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "11afb733-fd5a-4a60-a3c4-da0d3645147b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": -1,
                "shift": 13,
                "w": 13,
                "x": 115,
                "y": 76
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "338173d0-8263-49d2-b6c9-97b29f331758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 7,
                "x": 473,
                "y": 76
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0b877186-331e-4474-bcbf-4673ca9f181a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": -2,
                "shift": 8,
                "w": 6,
                "x": 496,
                "y": 76
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e8e4b3cd-f9e5-4a7e-98e1-b5eff8c9c152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": -1,
                "shift": 11,
                "w": 11,
                "x": 360,
                "y": 76
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0ad47e38-baca-4d12-81d4-08cffc2f60f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 14,
                "x": 206,
                "y": 76
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8bfc84d4-697e-49af-b836-44df32abfb8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 12,
                "x": 300,
                "y": 76
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cfa0e155-ae4f-464e-a366-7286f001a375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 14,
                "x": 34,
                "y": 76
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a48353e7-0f62-44a6-8431-54ac501cbc1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 29,
                "offset": -2,
                "shift": 18,
                "w": 19,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "09d04e40-32da-46a4-8d28-86c5959520b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": -2,
                "shift": 16,
                "w": 16,
                "x": 475,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "43a69057-d99c-47be-aad6-4ce309dd8982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": -2,
                "shift": 14,
                "w": 17,
                "x": 255,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8f641452-3224-4161-9073-ea1fbb522968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": -2,
                "shift": 13,
                "w": 15,
                "x": 322,
                "y": 39
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "aed2469c-3f5e-4f77-9a2a-621d8659ef47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 349,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f0274076-59ce-46d5-ba80-8481a85e2994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": -2,
                "shift": 12,
                "w": 15,
                "x": 407,
                "y": 39
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "78ea4c06-94eb-483f-a228-80cd9046c91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 18,
                "y": 76
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "099d514e-a4fb-4f57-855f-d14ce25fb342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": -2,
                "shift": 14,
                "w": 15,
                "x": 390,
                "y": 39
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3873b36c-aa6c-4033-b6c9-f87675e6c158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": -2,
                "shift": 14,
                "w": 17,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c3e4816e-cc63-43ed-abca-d9c689950d55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": -2,
                "shift": 7,
                "w": 9,
                "x": 453,
                "y": 76
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "55b1ca94-2bcf-403e-aa2f-0eab576f670d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": -2,
                "shift": 13,
                "w": 16,
                "x": 38,
                "y": 39
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "23034d51-a11d-41b8-b9fb-dec439990ceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 29,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 93,
                "y": 39
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6fb72aad-8d09-42f6-a356-e67538fe0f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 29,
                "offset": -2,
                "shift": 11,
                "w": 11,
                "x": 274,
                "y": 76
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5d88b562-1e15-4fd9-b0ff-2c1ecbbb8432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": -2,
                "shift": 19,
                "w": 23,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "80131040-0e6f-4d8d-8da6-1f5c019af82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": -2,
                "shift": 15,
                "w": 17,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b9c7b650-b0ee-43c9-857c-4bd57f9342ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 339,
                "y": 39
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "257dafae-da67-4b2d-81f5-e1557229c0e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 2,
                "y": 39
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3f65c5d4-3d2c-4d49-a058-fece257686dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 14,
                "x": 82,
                "y": 76
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d87eb8ff-eba2-457a-abe5-abaec5f70e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 29,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 75,
                "y": 39
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "34242f38-7e5d-4cad-b46e-593bf022b82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 29,
                "offset": -3,
                "shift": 13,
                "w": 15,
                "x": 475,
                "y": 39
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6636d182-6f35-42d0-b162-bf284b1baa0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 29,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 147,
                "y": 39
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "803cb760-e83e-46d8-be37-f1bf4c8a0049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 29,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 129,
                "y": 39
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8b7735e3-8bf9-465e-85e6-50c7a55ea398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 385,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "01b661d3-ff04-4dd5-bee4-ebd2d9eac0e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9ce7b5c0-83cf-4032-b76b-1d9ea4b96e5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": -2,
                "shift": 15,
                "w": 17,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e06214c9-d1dd-4404-9754-41309aae318a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": -2,
                "shift": 14,
                "w": 16,
                "x": 493,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a90a21de-e0ec-4321-a3cb-84c37b7c602a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": -2,
                "shift": 12,
                "w": 15,
                "x": 424,
                "y": 39
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cc104627-f802-473c-aeb1-7f352d367616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 261,
                "y": 76
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6bace2c3-5b68-4a0f-a27b-73d064772b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": -1,
                "shift": 11,
                "w": 11,
                "x": 248,
                "y": 76
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3415511c-e6f2-49aa-af40-0121eb192613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": -2,
                "shift": 10,
                "w": 11,
                "x": 235,
                "y": 76
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4541e874-06f8-46d8-95be-bc598da46c6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": -1,
                "shift": 14,
                "w": 13,
                "x": 394,
                "y": 76
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2e01bc6c-4311-4bcb-a5ab-862051d6e49d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 35,
                "offset": -1,
                "shift": 14,
                "w": 17,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6efafc0c-6f96-425f-998d-3c1b9aa5d810",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 11,
                "y": 113
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "15b7fc14-2890-4a0a-8369-04583eb9e4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": -2,
                "shift": 16,
                "w": 16,
                "x": 367,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "406220cc-d22e-4bde-bc60-c1bde30ba403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": -2,
                "shift": 14,
                "w": 17,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "96889e01-e137-4908-9c55-9d239b7b7822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": -2,
                "shift": 13,
                "w": 15,
                "x": 356,
                "y": 39
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "867d908d-6617-4263-88a5-9dfaf6cde3f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 421,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "443a12c4-0a95-4f82-aba2-af7220d2bbb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": -2,
                "shift": 12,
                "w": 15,
                "x": 288,
                "y": 39
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bdffa170-fb98-4cd7-80a3-ce0a06564a3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 2,
                "y": 76
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "40322861-3b20-44c3-a766-02e738dba8e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": -2,
                "shift": 14,
                "w": 15,
                "x": 271,
                "y": 39
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "22829ed9-c588-4c1e-b003-f76f77c14c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": -2,
                "shift": 14,
                "w": 17,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "99e95b74-f5df-400d-b58c-1e2da1474db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": -2,
                "shift": 7,
                "w": 9,
                "x": 420,
                "y": 76
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "28659f1a-a1c2-41c6-b472-65e959767f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -2,
                "shift": 13,
                "w": 16,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "000f8dd1-832e-4b0b-8335-65b5cdaadd50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 29,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 111,
                "y": 39
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "81078aca-1d2c-4cab-8d94-8f83d368a0b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 29,
                "offset": -2,
                "shift": 11,
                "w": 11,
                "x": 287,
                "y": 76
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e86aa64c-e836-411d-bf59-128281098c8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": -2,
                "shift": 19,
                "w": 23,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "dce6cb58-1080-4185-87bc-bffb8f60864e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": -2,
                "shift": 15,
                "w": 17,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "bee4187b-2f2a-4017-a434-d4016b096ba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 237,
                "y": 39
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2ae25ac1-90e2-4d36-8cf4-4395d03c9790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d4649454-5465-4c13-a699-f8349f85ce77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 14,
                "x": 66,
                "y": 76
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e04d9c1a-05f1-43dc-830f-3f49ad925b7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 29,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 165,
                "y": 39
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "89c72e1f-1b3a-4461-ab07-44402adf9504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 29,
                "offset": -3,
                "shift": 13,
                "w": 16,
                "x": 183,
                "y": 39
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5a1827d8-ce8a-4ccb-9788-c0a7e4d6b2d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 29,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 219,
                "y": 39
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bf6f9fa3-80b5-4c7f-8502-bd720a290e39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 29,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 201,
                "y": 39
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8e339e1e-9144-4ee9-b831-9a5edd5e1396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 20,
                "y": 39
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c81143c9-d106-4178-9e5b-1a293992f6d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "009c7491-1a03-4ca5-b327-4b04784b25d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": -2,
                "shift": 15,
                "w": 17,
                "x": 293,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "84b8d14a-c977-4126-950a-421c4cd92fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": -2,
                "shift": 14,
                "w": 16,
                "x": 403,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "03d402d3-9ba6-461c-a6d2-73cf47e51b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": -2,
                "shift": 12,
                "w": 15,
                "x": 254,
                "y": 39
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "07514afd-fc3a-484b-b27e-cb5786ed8eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": -2,
                "shift": 11,
                "w": 13,
                "x": 130,
                "y": 76
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4b2f95a1-7aff-47fd-9954-4f8a5e293d3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 383,
                "y": 76
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "81275743-4006-40c6-a95d-b1d5dda71351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": -2,
                "shift": 12,
                "w": 12,
                "x": 192,
                "y": 76
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8986a3b7-c6d8-4fdf-8f0c-324d9e742e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 326,
                "y": 76
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroup": 0
}