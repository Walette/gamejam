{
    "id": "a366a7da-fba5-4d74-898b-1e77d345b298",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_house_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1079,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1591adad-1c95-41e5-9918-a5432df0eeb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a366a7da-fba5-4d74-898b-1e77d345b298",
            "compositeImage": {
                "id": "2f1e1030-5946-4f63-b14d-b42a0c4bdb4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1591adad-1c95-41e5-9918-a5432df0eeb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5e69d1c-f4ee-4838-a059-81c79a7b2b40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1591adad-1c95-41e5-9918-a5432df0eeb4",
                    "LayerId": "b1925c5c-c51c-451d-a815-b7fcd46e771a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "b1925c5c-c51c-451d-a815-b7fcd46e771a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a366a7da-fba5-4d74-898b-1e77d345b298",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1080,
    "xorig": 31,
    "yorig": 30
}