{
    "id": "ca672e9c-a8c7-4e51-a9ae-9cd9f626abef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprt_timer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "bdfc42f5-c626-45ff-8baa-aa2bf7d8ff34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca672e9c-a8c7-4e51-a9ae-9cd9f626abef",
            "compositeImage": {
                "id": "478e761c-d996-4d36-ac50-5fc2115f6b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdfc42f5-c626-45ff-8baa-aa2bf7d8ff34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d475052-df22-4047-85ec-922cb1f5be0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdfc42f5-c626-45ff-8baa-aa2bf7d8ff34",
                    "LayerId": "7145b422-4342-4c9d-ae83-dded99047515"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7145b422-4342-4c9d-ae83-dded99047515",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca672e9c-a8c7-4e51-a9ae-9cd9f626abef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 69
}