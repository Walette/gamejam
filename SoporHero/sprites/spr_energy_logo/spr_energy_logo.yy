{
    "id": "d06dcd75-71fe-4059-9163-5f3eb1f8bcd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energy_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "145e1ad9-99d6-4848-b65e-981f91b6a522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d06dcd75-71fe-4059-9163-5f3eb1f8bcd2",
            "compositeImage": {
                "id": "5749ca30-bed3-44e0-b531-25067d511818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "145e1ad9-99d6-4848-b65e-981f91b6a522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ece233c-8646-4e96-a94d-97e65e144c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "145e1ad9-99d6-4848-b65e-981f91b6a522",
                    "LayerId": "c64e0192-4ff3-4dc2-981b-3752cc2f01f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c64e0192-4ff3-4dc2-981b-3752cc2f01f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d06dcd75-71fe-4059-9163-5f3eb1f8bcd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}