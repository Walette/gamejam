{
    "id": "3c3b5683-e95d-4258-9e50-8dda781d2094",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "edc9602b-7ab3-4e10-909e-66ed35d8463d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c3b5683-e95d-4258-9e50-8dda781d2094",
            "compositeImage": {
                "id": "8c03e694-a241-4f4d-8442-15cae5b735bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edc9602b-7ab3-4e10-909e-66ed35d8463d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c973a85-18bc-4d00-9261-79db43f8cc11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edc9602b-7ab3-4e10-909e-66ed35d8463d",
                    "LayerId": "d0fa5e10-0edb-47ae-ba33-f0a3b7df6234"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "d0fa5e10-0edb-47ae-ba33-f0a3b7df6234",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c3b5683-e95d-4258-9e50-8dda781d2094",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}