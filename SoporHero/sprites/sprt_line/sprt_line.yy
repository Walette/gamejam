{
    "id": "59f14869-7095-40b7-bdb9-a6e71d342f40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprt_line",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "46223e1c-debe-4674-92e0-ee6dd13877a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59f14869-7095-40b7-bdb9-a6e71d342f40",
            "compositeImage": {
                "id": "289c07ab-0e35-4119-a6a6-485688cfdcd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46223e1c-debe-4674-92e0-ee6dd13877a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0bb47f4-f1f0-4986-852b-2543bf8138ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46223e1c-debe-4674-92e0-ee6dd13877a3",
                    "LayerId": "9667d553-d057-41ac-a8fa-ddcac8a8998e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9667d553-d057-41ac-a8fa-ddcac8a8998e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59f14869-7095-40b7-bdb9-a6e71d342f40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 65
}