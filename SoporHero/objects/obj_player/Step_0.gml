/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 05B6660B
/// @DnDArgument : "code" "/// @description Execute Code\n$(13_10)if (score == 0)$(13_10){$(13_10)	score = 1;$(13_10)	room_goto(rm_menu);$(13_10)}$(13_10)$(13_10)if (mySec == 0)$(13_10){$(13_10)	defeat = 1;$(13_10)	score = 3;$(13_10)	room_goto(rm_menu);$(13_10)}"

{
	/// @description Execute Code\n
if (score == 0)
{
	score = 1;
	room_goto(rm_menu);
}

if (mySec == 0)
{
	defeat = 1;
	score = 3;
	room_goto(rm_menu);
}
}

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 2D28222E
/// @DnDArgument : "code" "if (defeat == 0)$(13_10){$(13_10)if (boolCana == 0)$(13_10){$(13_10)	key_up = keyboard_check(vk_up);$(13_10)	key_down  = keyboard_check(vk_down);$(13_10)	key_left = keyboard_check(vk_left);$(13_10)	key_right = keyboard_check(vk_right);$(13_10)}$(13_10)else$(13_10){$(13_10)	key_up = keyboard_check(vk_right);$(13_10)	key_down  = keyboard_check(vk_left);$(13_10)	key_left = keyboard_check(vk_up);$(13_10)	key_right = keyboard_check(vk_down);$(13_10)}$(13_10)$(13_10)if (!key_up && !key_down && !key_left && !key_right && dsound == 1)$(13_10){$(13_10)	audio_pause_sound(sound_3);$(13_10)	dsound = 0;$(13_10)}$(13_10)else if (dsound == 0)$(13_10){$(13_10)	dsound = 1;$(13_10)	audio_play_sound(sound_3, 0, 1);$(13_10)}$(13_10)$(13_10)// [Horizontal]$(13_10)// Deceleration$(13_10)	if (key_left == 0 && key_right == 0)$(13_10)	{$(13_10)		if (spd_h > 1)$(13_10)		{$(13_10)			spd_h -= 0.5;$(13_10)		}$(13_10)		else if (spd_h < -1)$(13_10)		{$(13_10)			spd_h += 0.5;$(13_10)		}$(13_10)		else {$(13_10)			spd_h = 0;$(13_10)		}$(13_10)	}$(13_10)$(13_10)	// Accélération $(13_10)	cl_spd = (sqrt(spd_h * spd_h ) + sqrt(spd_v * spd_v)) / 2;$(13_10)	if (cl_spd >= ((1 * max_spd) / 4))$(13_10)	{$(13_10)		acc += 0.2;$(13_10)	}$(13_10)	else if (cl_spd >= ((2 * max_spd) / 4))$(13_10)	{$(13_10)		acc += 0.4;$(13_10)	}$(13_10)	else if (cl_spd >= ((3 * max_spd) / 4))$(13_10)	{$(13_10)		acc += 0.6;$(13_10)	}$(13_10)	else $(13_10)	{$(13_10)		acc = 0.5;$(13_10)	}$(13_10)$(13_10)// Calcul de la vitesse$(13_10)	if (key_right)$(13_10)	{$(13_10)		if (hface == -1)$(13_10)			acc = 0.5;$(13_10)		if (spd_h < max_spd)$(13_10)		{$(13_10)			spd_h += acc;$(13_10)		}$(13_10)		hface = 1;$(13_10)	}$(13_10)$(13_10)	if (key_left)$(13_10)	{$(13_10)		if (hface == 1)$(13_10)			acc = 0.5;$(13_10)		if (spd_h > -max_spd)$(13_10)		{$(13_10)			spd_h -= acc;$(13_10)		}$(13_10)		hface = -1;$(13_10)	}$(13_10)$(13_10)// [Vertical]$(13_10)// Deceleration$(13_10)	if (key_up == 0 && key_down == 0)$(13_10)	{$(13_10)	if (spd_v > 1)$(13_10)	{$(13_10)		spd_v -= 0.5;$(13_10)	}$(13_10)	else if (spd_v < -1)$(13_10)	{$(13_10)		spd_v += 0.5;$(13_10)	}$(13_10)	else {$(13_10)		spd_v = 0;$(13_10)	}$(13_10)}$(13_10)$(13_10)// Calcul de la vitesse$(13_10)if (key_down)$(13_10){$(13_10)	if (vface == -1)$(13_10)		acc = 0.5;$(13_10)	if (spd_v < max_spd)$(13_10)	{$(13_10)		spd_v += acc;$(13_10)	}$(13_10)	hface = 1;$(13_10)}$(13_10)$(13_10)if (key_up)$(13_10){$(13_10)	if (vface == 1)$(13_10)		acc = 0.5;$(13_10)	if (spd_v > -max_spd)$(13_10)	{$(13_10)		spd_v -= acc;$(13_10)	}$(13_10)	vface = -1;$(13_10)}$(13_10)$(13_10)// Assignation$(13_10)if (instance_place(x + spd_h, y, obj_wall))$(13_10)	spd_h *= -1;$(13_10)if (instance_place(x, y + spd_v, obj_wall))$(13_10)	spd_v *= -1;	$(13_10)if (x + spd_h > 30 && x + spd_h < room_width - 30)$(13_10)	x += spd_h;$(13_10)if (y + spd_v > 30 && y + spd_v < room_height - 30)$(13_10)	y += spd_v;$(13_10)$(13_10)//Direction du tir$(13_10)if (key_up)$(13_10)	last_key = 0;$(13_10)else if (key_right)$(13_10)	last_key = 1;$(13_10)else if (key_down)$(13_10)	last_key = 2;$(13_10)else if (key_left)$(13_10)	last_key = 3;$(13_10)$(13_10)if (keyboard_check_pressed(vk_space))$(13_10){$(13_10)	instance_create_layer(x, y, "Instances", obj_collision);$(13_10)}$(13_10)$(13_10)}"

{
	if (defeat == 0)
{
if (boolCana == 0)
{
	key_up = keyboard_check(vk_up);
	key_down  = keyboard_check(vk_down);
	key_left = keyboard_check(vk_left);
	key_right = keyboard_check(vk_right);
}
else
{
	key_up = keyboard_check(vk_right);
	key_down  = keyboard_check(vk_left);
	key_left = keyboard_check(vk_up);
	key_right = keyboard_check(vk_down);
}

if (!key_up && !key_down && !key_left && !key_right && dsound == 1)
{
	audio_pause_sound(sound_3);
	dsound = 0;
}
else if (dsound == 0)
{
	dsound = 1;
	audio_play_sound(sound_3, 0, 1);
}

// [Horizontal]
// Deceleration
	if (key_left == 0 && key_right == 0)
	{
		if (spd_h > 1)
		{
			spd_h -= 0.5;
		}
		else if (spd_h < -1)
		{
			spd_h += 0.5;
		}
		else {
			spd_h = 0;
		}
	}

	// Accélération 
	cl_spd = (sqrt(spd_h * spd_h ) + sqrt(spd_v * spd_v)) / 2;
	if (cl_spd >= ((1 * max_spd) / 4))
	{
		acc += 0.2;
	}
	else if (cl_spd >= ((2 * max_spd) / 4))
	{
		acc += 0.4;
	}
	else if (cl_spd >= ((3 * max_spd) / 4))
	{
		acc += 0.6;
	}
	else 
	{
		acc = 0.5;
	}

// Calcul de la vitesse
	if (key_right)
	{
		if (hface == -1)
			acc = 0.5;
		if (spd_h < max_spd)
		{
			spd_h += acc;
		}
		hface = 1;
	}

	if (key_left)
	{
		if (hface == 1)
			acc = 0.5;
		if (spd_h > -max_spd)
		{
			spd_h -= acc;
		}
		hface = -1;
	}

// [Vertical]
// Deceleration
	if (key_up == 0 && key_down == 0)
	{
	if (spd_v > 1)
	{
		spd_v -= 0.5;
	}
	else if (spd_v < -1)
	{
		spd_v += 0.5;
	}
	else {
		spd_v = 0;
	}
}

// Calcul de la vitesse
if (key_down)
{
	if (vface == -1)
		acc = 0.5;
	if (spd_v < max_spd)
	{
		spd_v += acc;
	}
	hface = 1;
}

if (key_up)
{
	if (vface == 1)
		acc = 0.5;
	if (spd_v > -max_spd)
	{
		spd_v -= acc;
	}
	vface = -1;
}

// Assignation
if (instance_place(x + spd_h, y, obj_wall))
	spd_h *= -1;
if (instance_place(x, y + spd_v, obj_wall))
	spd_v *= -1;	
if (x + spd_h > 30 && x + spd_h < room_width - 30)
	x += spd_h;
if (y + spd_v > 30 && y + spd_v < room_height - 30)
	y += spd_v;

//Direction du tir
if (key_up)
	last_key = 0;
else if (key_right)
	last_key = 1;
else if (key_down)
	last_key = 2;
else if (key_left)
	last_key = 3;

if (keyboard_check_pressed(vk_space))
{
	instance_create_layer(x, y, "Instances", obj_collision);
}

}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 6F94DF13
/// @DnDArgument : "var" "boolExtasy"
/// @DnDArgument : "value" "1"
if(boolExtasy == 1)
{
	

	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 078024E6
	/// @DnDParent : 6F94DF13
	/// @DnDArgument : "imageind_relative" "1"
	/// @DnDArgument : "spriteind" "spr_bird"
	/// @DnDSaveInfo : "spriteind" "7c2abc91-8ae7-4e38-a4ea-294f03c596cf"
	sprite_index = spr_bird;
	image_index += 0;


}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 4C9BBF20
else
{
	

	/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
	/// @DnDVersion : 1
	/// @DnDHash : 144D7369
	/// @DnDParent : 4C9BBF20
	/// @DnDArgument : "key" "vk_down"
	var l144D7369_0;
	l144D7369_0 = keyboard_check(vk_down);
	if (l144D7369_0)
	{
		
	
			/// @DnDAction : YoYo Games.Instances.Set_Sprite
			/// @DnDVersion : 1
			/// @DnDHash : 0777AA43
			/// @DnDParent : 144D7369
			/// @DnDArgument : "imageind_relative" "1"
			/// @DnDArgument : "spriteind" "spr_player_front"
			/// @DnDSaveInfo : "spriteind" "0fc3cffb-e859-4e2d-9d5a-da3e0c1b6828"
			sprite_index = spr_player_front;
			image_index += 0;
	
	
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 0F380496
	/// @DnDParent : 4C9BBF20
	else
	{
		
	
			/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
			/// @DnDVersion : 1
			/// @DnDHash : 50434391
			/// @DnDParent : 0F380496
			/// @DnDArgument : "key" "vk_left"
			var l50434391_0;
			l50434391_0 = keyboard_check(vk_left);
			if (l50434391_0)
			{
				
			
						/// @DnDAction : YoYo Games.Instances.Set_Sprite
						/// @DnDVersion : 1
						/// @DnDHash : 6309B820
						/// @DnDParent : 50434391
						/// @DnDArgument : "imageind_relative" "1"
						/// @DnDArgument : "spriteind" "spr_player_left"
						/// @DnDSaveInfo : "spriteind" "bcf4f5cf-d17a-43cb-9c71-907aef49b67e"
						sprite_index = spr_player_left;
						image_index += 0;
			
			
			}
	
			/// @DnDAction : YoYo Games.Common.Else
			/// @DnDVersion : 1
			/// @DnDHash : 69115C24
			/// @DnDParent : 0F380496
			else
			{
				
			
						/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
						/// @DnDVersion : 1
						/// @DnDHash : 4F403778
						/// @DnDParent : 69115C24
						/// @DnDArgument : "key" "vk_right"
						var l4F403778_0;
						l4F403778_0 = keyboard_check(vk_right);
						if (l4F403778_0)
						{
							
						
										/// @DnDAction : YoYo Games.Instances.Set_Sprite
										/// @DnDVersion : 1
										/// @DnDHash : 565194E0
										/// @DnDParent : 4F403778
										/// @DnDArgument : "imageind_relative" "1"
										/// @DnDArgument : "spriteind" "spr_player_right"
										/// @DnDSaveInfo : "spriteind" "b811eaf4-0355-4d54-9e68-866a3b8b841c"
										sprite_index = spr_player_right;
										image_index += 0;
						
						
						}
			
						/// @DnDAction : YoYo Games.Common.Else
						/// @DnDVersion : 1
						/// @DnDHash : 6E93E01A
						/// @DnDParent : 69115C24
						else
						{
							
						
										/// @DnDAction : YoYo Games.Instances.Set_Sprite
										/// @DnDVersion : 1
										/// @DnDHash : 67FC69CD
										/// @DnDParent : 6E93E01A
										/// @DnDArgument : "imageind_relative" "1"
										/// @DnDArgument : "spriteind" "spr_player"
										/// @DnDSaveInfo : "spriteind" "94f64b40-e233-48c6-a6ee-121974ac016a"
										sprite_index = spr_player;
										image_index += 0;
						
						
						}
			
			
			}
	
	
	}


}

