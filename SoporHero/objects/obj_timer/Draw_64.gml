/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 3E3C173B
/// @DnDArgument : "code" "// Display text //$(13_10)$(13_10)draw_set_font(font_timer);$(13_10)draw_set_color(c_red);$(13_10)draw_sprite(sprt_timer, 0, x + 18, y + 3);$(13_10)draw_sprite_ext(sprt_line, 0, posX, posY, 1, 1, angleSec, c_white, 1);$(13_10)$(13_10)if (myMin >= 1)$(13_10){$(13_10)	if (showTime % 60 >= 10)$(13_10)	{$(13_10)		draw_text(x, y + 40, string(myMin) + ":" + string(showTime % 60));$(13_10)	}$(13_10)	else$(13_10)	{$(13_10)		draw_text(x, y + 40, string(myMin) + ":0" + string(showTime % 60));$(13_10)	}$(13_10)}$(13_10)else$(13_10){$(13_10)	draw_text(x + 5, y + 40, string(showTime % 60));$(13_10)}$(13_10)$(13_10)// Display Actual Item //$(13_10)$(13_10)draw_set_color(c_red);$(13_10)draw_text(40, 575, "Objet a trouver");$(13_10)draw_sprite(sprt_actual[object], 0, 100, 625);$(13_10)$(13_10)/// Code energy_bar ///$(13_10)$(13_10)draw_sprite(spr_energy_logo, 0, 990, 60);$(13_10)draw_healthbar(985, 97, 1000, 651, energy_value, c_dkgray, c_lime, c_red, 3, true, true)$(13_10)$(13_10)"

{
	// Display text //

draw_set_font(font_timer);
draw_set_color(c_red);
draw_sprite(sprt_timer, 0, x + 18, y + 3);
draw_sprite_ext(sprt_line, 0, posX, posY, 1, 1, angleSec, c_white, 1);

if (myMin >= 1)
{
	if (showTime % 60 >= 10)
	{
		draw_text(x, y + 40, string(myMin) + ":" + string(showTime % 60));
	}
	else
	{
		draw_text(x, y + 40, string(myMin) + ":0" + string(showTime % 60));
	}
}
else
{
	draw_text(x + 5, y + 40, string(showTime % 60));
}

// Display Actual Item //

draw_set_color(c_red);
draw_text(40, 575, "Objet a trouver");
draw_sprite(sprt_actual[object], 0, 100, 625);

/// Code energy_bar ///

draw_sprite(spr_energy_logo, 0, 990, 60);
draw_healthbar(985, 97, 1000, 651, energy_value, c_dkgray, c_lime, c_red, 3, true, true)


}

