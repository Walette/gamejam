{
    "id": "bec31cd6-c7d8-486c-90cb-a7d89f59f41f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_timer",
    "eventList": [
        {
            "id": "90af2764-9322-45b0-9468-fe9e6a28752b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bec31cd6-c7d8-486c-90cb-a7d89f59f41f"
        },
        {
            "id": "c6d090a7-b9b0-4c37-97fb-48cd7cf8795a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bec31cd6-c7d8-486c-90cb-a7d89f59f41f"
        },
        {
            "id": "5bded1d6-5d9f-4812-b325-16b05af9e4fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "bec31cd6-c7d8-486c-90cb-a7d89f59f41f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}